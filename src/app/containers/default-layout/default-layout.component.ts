import {Component} from '@angular/core';
import {navItems} from '../../_nav';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public sidebarMinimized = false;
  public navItems = navItems;
  private changes: MutationObserver;

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.observeSideBar();
  }

  private observeSideBar() {
    this.changes = new MutationObserver((mutations: MutationRecord[]) => {
      setTimeout(() => {
        window.dispatchEvent(new Event('resize'));
      }, 300);
    });

    this.changes.observe(document.body, {
      attributes: true,
      attributeOldValue: true,
      attributeFilter: ['class'],
    });
  }

  ngOnDestroy() {
    this.changes.disconnect();
  }

  toggleMinimize(e) {
    debugger;
    this.sidebarMinimized = e;
    // setTimeout(() => {
    //   window.dispatchEvent(new Event('resize'));
    // },200)
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

  public domChange($event: any) {
    console.log($event);
  }
}
