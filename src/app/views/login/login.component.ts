import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  public username: string;
  public password: string;
  public error: string;
  private returnUrl: string;

  constructor(
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.queryParams.subscribe(params => {
      this.returnUrl = params.returnUrl;
    })
  }

  submit() {
    if (!this.username || !this.password) {
      this.error = 'Veuillez renseigner vos identifiants';
      return;
    }

    this.authService.login(this.username, this.password).subscribe(res => {
      this.router.navigateByUrl(this.returnUrl || '/');
    }, error => {
      if (error === 'Unauthorized') {
        this.error = 'Identifiants incorrects';
      } else {
        this.error = 'Erreur inconnue';
      }
    })
  }
}
