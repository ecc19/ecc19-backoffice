import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OfferingListComponent} from './offering-list/offering-list.component';

const routes: Routes = [
  {
    path: '',
    component: OfferingListComponent,
    data: {
      title: 'Donations'
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfferingRoutingModule {}
