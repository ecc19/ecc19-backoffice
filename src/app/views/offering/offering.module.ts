import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OfferingListComponent} from './offering-list/offering-list.component';
import {OfferingRoutingModule} from './offering-routing.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [OfferingListComponent],
  imports: [
    CommonModule,
    OfferingRoutingModule,
    NgxDatatableModule
  ]
})
export class OfferingModule { }
