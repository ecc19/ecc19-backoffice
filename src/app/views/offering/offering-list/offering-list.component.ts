import {Component, OnInit, ViewChild} from '@angular/core';
import {ColumnMode} from '@swimlane/ngx-datatable';
import {OfferingService} from '../../../services/offering.service';

@Component({
  selector: 'app-offering-list',
  templateUrl: './offering-list.component.html',
  styleUrls: ['./offering-list.component.css']
})
export class OfferingListComponent implements OnInit {
  @ViewChild('table') table: any;

  rows;
  ColumnMode = ColumnMode;

  constructor(
    private offeringService: OfferingService,
  ) {
    this.offeringService.list().subscribe(res => {
      this.rows = res.items;
    });
  }

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  ngOnInit(): void {
  }

  onActivate($event: any) {
    if ( $event.type == "click" ) {
      console.log($event.row);
    }
  }

  public onDetailToggle($event: any) {
    console.log('Detail Toggled', event);
  }
}
