import {INavData} from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Tableau de bord',
    url: '/dashboard',
    icon: 'icon-speedometer',
  },
  {
    title: true,
    name: 'Dons'
  },
  {
    name: 'Liste des dons',
    url: '/offerings',
    icon: 'icon-list'
  },
  {
    title: true,
    name: 'Administration'
  },
  {
    name: 'Utilisateurs',
    url: '/users',
    icon: 'icon-people',
  }
];
