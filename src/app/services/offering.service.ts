import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {PaginationDto} from '../models/pagination.dto';
import {Offering} from '../models/offering';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OfferingService {
  constructor(
    private httpClient: HttpClient
  ) {

  }

  list(): Observable<PaginationDto<Offering>> {
    return this.httpClient.get<PaginationDto<Offering>>(`${environment.apiUrl}/offerings`);
  }
}
