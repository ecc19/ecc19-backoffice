export class User {
  id: number;
  createdAt: Date;
  username: string;
  email: string;
  password: string;
  roles: string;
}
