import {OfferingItem} from './offering-item';

export class Offering {
  constructor() {}

  id: string;
  items: OfferingItem[] = [];
  name: string;
  company: string;
  email: string;
  phone: string;
}
